/*
create table test (
        id varchar(255) primary key,
        x integer
);

insert into test values ('a', 1);
insert into test values ('b', 2);
insert into test values ('c', 3);

select * from test;

select * from test where id='b';

update test set x = 22 where id='b';

select * from test;

delete from test where id='b';

select * from test;
*/


//setup
const pg = require('pg');
process.env.PGDATABASE = 'pg';
const pool = new pg.Pool();
pool.on('error', (err, client) => {
  console.log(err.stack);
  process.exit(-1);
});


// Get a database connection for the pool.
pool.connect((err, client, done) => {
  if (err) throw err;
  createTest(client, done);
});

// Create table test.
function createTest(client, done) {
  const q = 'create table test (            ' +
            '  id varchar(255) primary key, ' +
            '  x integer                    ' +
            ')                              ';
  client.query(q, (err) => {
    if (err) throw err;
    else insertA(client, done);
  });
}

function insertRow(client, id, x, cb) {
   client.query("insert into test values ($1::text, $2)", [id, x], (err) => {
     if (err) throw err; else cb();
   });
}

function insertA(client, done) {
  insertRow(client, 'a', 1, () => {
    insertB(client, done);
  });
}

function insertB(client, done) {
  insertRow(client, 'b', 2, () => {
    insertC(client, done);
  });
}

function insertC(client, done) {
  insertRow(client, 'c', 3, () => {
    selectAll(client, done, () => { done(); pool.end();
    });
  });
}

function updateB(client, done) {
    updateRow(client, 'b', 22, () => {
        selectAll(client, done);
    });
}

function deleteB(client, done) {
    deleteRow(client, 'b', () => {
        selectAll(client, done);
        done();
        pool.end();
    });
}

function selectAll(client, cb) {
  client.query("select * from test order by id", (err, result) => {
    if (err) throw err;
    result.rows.map((row, i) => {
      console.log('  ' + i + " " + row.id + ' ' + row.x);
    });
    cb();
  });
}

//set x for given id
function updateRow(client, id, x, cb) {
    client.query("update test set x = $1 where id=$2::text" [x, id], (err) => {
        if (err) throw err;
        cb();
    });
}

//delete row for given id
function deleteRow(client, id, cb) {
    client.query("delete from test where id = $1::text", [id], (err) => {
        if (err) throw err;
        cb();
    });
}
