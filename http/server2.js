const http = require('http');

const server = http.createServer((req, res) => {
    res.setHeader('Content-Type', 'text/html');
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end(html);
});

server.listen(8000, () => {
    console.log('Waiting for client...');
    console.log('');
});

const html =
    "<html>"                           +
    "<head><title>405</title></head>"  +
    "<body><h1>hi</h1></body>"         +
    "</html>";

server.on('connection', (socket) => {
    console.log('Connected!!');
    socket.on('data', (data) => {
        console.log('Received from client:\n' + data);
        socket.end(html);
    });
});
