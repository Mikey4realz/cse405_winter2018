create table test (
        id varchar(255) primary key,
        x integer
);

insert into test values ('a', 1);
insert into test values ('b', 2);
insert into test values ('c', 3);

select * from test;

select * from test where id='b';

update test set x = 22 where id='b';

select * from test;

delete from test where id='b';

select * from test;
