const http = require('http');
const st = require('st');

const port = 8000;

var mount = st({
  path: 'public/',
  index: 'index.html'
});

const html = "<html><div>You requested /hi</div></html>";


const server = http.createServer((req, res) => {
    if (req.url == '/hi') {
        console.log('/hi being requested');
        res.writeHeader(200, {"Content-Type": "text/html"});
        res.write(html);
        res.end();
    }
    else {
        mount(req, res);
    }
});

server.listen(port, () => {
        console.log('Server running at http://localhost:${port}/');
});
